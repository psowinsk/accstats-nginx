# nginx proxy on openshift

### This is Based on OpenShift example projects: 
- [nginx-ex](https://github.com/sclorg/nginx-ex) for README and OpenShift template
- [nginx-container](https://github.com/sclorg/nginx-container) for example project
- example of [nginx configurations](https://github.com/sclorg/nginx-container/tree/master/examples/1.16/test-app) 

## Instructions 
### Create website (only the first time)
- [Create CERN web site](https://webservices.web.cern.ch/webservices/Services/ManageSite/)

### Build and Deploy on OpenShift
```
oc login https://openshift.cern.ch      # openshift-dev for test web sites
oc project [project-name], e.g. acc-stats
oc new-app centos/nginx-116-centos7:latest~https://gitlab.cern.ch/psowinsk/accsstats-nginx.git
oc expose svc/accstats-nginx --port=8080
oc status
``` 

### Create route as shown:
[Adding route](https://gitlab.cern.ch/psowinsk/accstats-nginx/-/blob/master/route.png)


## Remove proxy server (if neccessary):
```
oc delete all --selector app=accstats-nginx
oc status
```
check stats on [openshift web console](https://openshift.cern.ch/console/project/acc-stats/overview)

